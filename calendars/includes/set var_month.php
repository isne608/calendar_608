<?php
 /* Set variable to set up calendar page */


	$today = date('d'); 
	$todaymonth = date('m'); 
	$todayyear = date('Y');


	if(isset($_REQUEST['date'])){
		$day = date('d', strtotime($_REQUEST['date']));
		$month = date('m', strtotime($_REQUEST['date']));
		$year = date('Y', strtotime($_REQUEST['date']));
		$days = date('t', strtotime($_REQUEST['date']));
		$nmonth = strtotime($_REQUEST['date']);
		$date = date('Y-m-d', strtotime($_REQUEST['date']));
		
		$starter = 0;
	}
	else{
		
		$date = date('Y-m-d');
		$day = $today;
		$month = $todaymonth;
		$year = $todayyear;
		$days = date('t', strtotime($date));
		$nmonth = strtotime($date);

		$starter = 1;
	}

	if(isset($_REQUEST['dateGo'])){
		$select_day = date('d', strtotime($_REQUEST['dateGo'])); 
		$select_month = date('m', strtotime($_REQUEST['dateGo'])); 
		$select_year = date('Y', strtotime($_REQUEST['dateGo'])); 

		$day = date('d', strtotime($_REQUEST['dateGo'])); 
		$month = date('m', strtotime($_REQUEST['dateGo'])); 
		$year = date('Y', strtotime($_REQUEST['dateGo'])); 
		$days = date('t', strtotime($_REQUEST['dateGo'])); 
		$nmonth = strtotime($_REQUEST['dateGo']);
		$date = date('Y-m-d', strtotime($_REQUEST['dateGo']));	
	}

	if(isset($_REQUEST['title'])){
		$title = $_REQUEST['title'];
	}

	if(isset($_REQUEST['details'])){
		$details = $_REQUEST['details'];
	}
	else{
		$details = '';
	}

	if(isset($_REQUEST['start'])){
		$start = $_REQUEST['start'];
		$end = $_REQUEST['end'];		
	}

	$firstday = date('w', strtotime('01-' . $month . '-' . $year));
	//the month. (e.g. 0 for Sun, 1 for Mon)
				
	$monthName = date("F", mktime(null, null, null, $month)); //change number to name month
	$nextM = date('Y-m-d',strtotime('+1 month', $nmonth));
	$prevM = date('Y-m-d',strtotime('-1 month', $nmonth));
?>